# Yapilacaklar

- Twig template engine entegre edilecek
- NotORM entegre edilecek
- On taraf icin hazirlanan template twig ile uyumlu hale getirilecek
- Rotalar hazilanacak

```
@GET         /

# Authentication
@GET         /auth/login
@POST        /auth/login
@GET         /auth/register
@POST        /auth/register
@DELETE      /auth/logout

# Videolar 
@GET         /videos
@GET         /videos/<id>
@POST        /videos
@GET         /videos/category/<category_id>

# Yorum ekle
@POST        /videos/<id>/comments

# Kullanici profili
@GET         /users/<user_id>
@GET         /users/<user_id>/edit
@PUT         /users/<user_id>

# Admin video duzenlemeleri
@GET         /admin
@GET         /admin/videos/
@GET         /admin/videos/<id>
@GET         /admin/videos/<id>/edit
@PUT         /admin/videos/<id>
@DELETE      /admin/videos/<id>

# Video yorumlari duzenleme
@PUT         /admin/videos/<id>/comments/<id>
@DELETE      /admin/videos/<id>/comments/<id>

# Kullanici duzenleme
@GET         /admin/users
@GET         /admin/users/<id>
@GET         /admin/users/<id>/edit
@PUT         /admin/users/<id>
@DELETE      /admin/users/<id>

# Statik sayfalar
@GET         /about
@GET         /contact
```

- Veritabani yapisi

```
| users                    | 
| -------------------------|
| id         (int)         |
| username   (varchar:50)  |
| email      (varchar:255) |
| password   (varchar:255) |
| first_name (varchar:50)  |
| last_name  (varchar:50)  |
| is_admin   (tinyint:1)   |
| bio        (text)        |
| timestamps               |
|--------------------------|


| videos                        |
| ------------------------------|
| id              (int)         |
| title           (varchar:255) |
| slug            (varchar:255) |
| description     (text)        |
| user_id         (int)         |
| path            (varchar:255) |
| mime_type       (varchar:100) |
| extension       (varchar:20)  |
| thumbnail_path  (varchar:255) |
| view_count      (int)         |
| is_active       (tinyint:1)   |
| category_id     (int)         |
| timestamps                    |
|-------------------------------|

| categories               |
|--------------------------|
| id         (int)         |
| name       (varchar:255) |
| slug       (varchar:255) |
| timestamps               |
|--------------------------|

| comments                     |
| -----------------------------|
| id        (int)              |
| user_id   (int)              |
| video_id  (int)              |
| content   (text)             |
| timestamps                   |
|------------------------------|

```

- [Bootstrap Admin](http://startbootstrap.com/template-overviews/sb-admin-2/) Yonetim paneli icin kullanilacak

